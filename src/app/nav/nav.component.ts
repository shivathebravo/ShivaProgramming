import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onNavigate() {
    // your logic here.... like set the url 
    const url = 'http://shivaprogramming.com/blog/';
    window.open(url, '_blank');
  }

}
